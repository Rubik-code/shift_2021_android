package com.example.myweather.domain

interface CitesRepository {
    fun getListCites(): List<City>
    fun getCityAt(index: Int?): City?
}