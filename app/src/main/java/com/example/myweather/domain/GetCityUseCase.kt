package com.example.myweather.domain

class GetCityUseCase(private val repository: CitesRepository) {
    operator fun invoke(index: Int?): City? = repository.getCityAt(index)
}