package com.example.myweather.domain

class GetListCitesUseCase(private val repository: CitesRepository) {
    operator fun invoke(): List<City> = repository.getListCites()
}