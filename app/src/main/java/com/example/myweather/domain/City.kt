package com.example.myweather.domain

data class City (    val imageResource: Int,
                     val name: String,
                     val temperature: Int,
                     val windSpeed: Int,
                     val humidity: Int,
                     val weatherType: String)