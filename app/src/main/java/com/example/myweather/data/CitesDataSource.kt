package com.example.myweather.data

import com.example.myweather.domain.City

interface CitesDataSource {
    fun getListCites(): List<City>
    fun getCityAt(index: Int?): City?
}