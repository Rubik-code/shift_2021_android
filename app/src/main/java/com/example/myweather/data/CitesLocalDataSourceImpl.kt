package com.example.myweather.data

import com.example.myweather.R
import com.example.myweather.domain.City

class CitesLocalDataSourceImpl : CitesDataSource {

    private val listCites: List<City> = listOf(
    City(getImageResourceIdByWeatherType("Snow"), "Novosibirsk", -18, 6, 69, "Snow"),
    City(getImageResourceIdByWeatherType("Cloudy"), "Novokuznetsk", -12, 2, 62, "Cloudy"),
    City(getImageResourceIdByWeatherType("Sunny"), "Tomsk", -20, 2, 64, "Sunny"),
    City(getImageResourceIdByWeatherType("Chance of Snow"), "Omsk", -16, 7, 72, "Chance of Snow"),
    City(getImageResourceIdByWeatherType("Sunny"), "Tyumen", -15, 5, 70, "Sunny")
    )

    private fun getImageResourceIdByWeatherType(weatherType: String): Int =
            when (weatherType) {
            "Cloudy" -> R.drawable.ic__06_cloudy
            "Fog" -> R.drawable.ic__16_haze
            "Clear" -> R.drawable.ic__28_rainbow
            "Haze" -> R.drawable.ic__16_haze
            "Light Rain" -> R.drawable.ic__27_rain
            "Mostly Cloudy" -> R.drawable.ic__04_clouds
            "Overcast" -> R.drawable.ic__04_clouds
            "Partly Cloudy" -> R.drawable.ic__03_cloud
            "Rain" -> R.drawable.ic__27_rain
            "Rain Showers" -> R.drawable.ic__29_raindrop
            "Showers" -> R.drawable.ic__29_raindrop
            "Thunderstorm" -> R.drawable.ic__41_thunderstorm
            "Chance of Showers" -> R.drawable.ic__27_rain
            "Chance of Snow" -> R.drawable.ic__33_snowy
            "Snow" -> R.drawable.ic__32_snowy
            "Chance of Storm" -> R.drawable.ic__40_thunder
            "Mostly Sunny" -> R.drawable.ic__07_cloudy_day
            "Partly Sunny" -> R.drawable.ic__07_cloudy_day
            "Scattered Showers" -> R.drawable.ic__29_raindrop
            "Sunny" -> R.drawable.ic__36_sun
            else -> -1
            }

    override fun getListCites(): List<City> = listCites

    override fun getCityAt(index: Int?): City? = listCites[index!!]
}