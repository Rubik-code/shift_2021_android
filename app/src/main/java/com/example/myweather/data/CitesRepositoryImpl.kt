package com.example.myweather.data

import com.example.myweather.domain.CitesRepository
import com.example.myweather.domain.City

class CitesRepositoryImpl(private val citesDataSource: CitesDataSource) : CitesRepository {


    override fun getListCites(): List<City> = citesDataSource.getListCites()

    override fun getCityAt(index: Int?): City? = citesDataSource.getCityAt(index)
}