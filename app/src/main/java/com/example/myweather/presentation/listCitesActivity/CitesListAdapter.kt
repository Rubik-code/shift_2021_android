package com.example.myweather.presentation.listCitesActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myweather.domain.City
import com.example.myweather.R

class CitesListAdapter(private  val citesList: List<City>, private val listener: OnItemClickListener): RecyclerView.Adapter<CitesListAdapter.CitesListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitesListViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)

        return CitesListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CitesListViewHolder, position: Int) {
        val currentCity = citesList[position]

        holder.textViewCityName.text = currentCity.name
        holder.textViewTemperature.text = currentCity.temperature.toString() + '°'
        holder.imageView.setImageResource(currentCity.imageResource)
    }


    override fun getItemCount() = citesList.size

    inner class CitesListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val imageView: ImageView = itemView.findViewById(R.id.image_view)
        val textViewCityName: TextView = itemView.findViewById(R.id.city_name)
        val textViewTemperature: TextView = itemView.findViewById(R.id.temperature)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if(position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}