package com.example.myweather.presentation.presenters

import com.example.myweather.domain.GetCityUseCase
import com.example.myweather.presentation.currentCityActivity.CurrentCityView

class CurrentCityPresenter(private val getCityUseCase: GetCityUseCase, private val cityId: Int?) : BasePresenter<CurrentCityView>() {
    override fun onViewAttached() {
        val city = getCityUseCase(cityId)

        if (city != null) {
            view?.bindCity(city)
        } else {
            view?.closeScreen()
        }

    }
}