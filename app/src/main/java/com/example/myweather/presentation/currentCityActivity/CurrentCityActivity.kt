package com.example.myweather.presentation.currentCityActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.example.myweather.R
import com.example.myweather.data.CitesRepositoryImpl
import com.example.myweather.domain.City
import com.example.myweather.presentation.presenters.CurrentCityPresenter

class CurrentCityActivity : AppCompatActivity(), CurrentCityView {

    private val presenter by lazy {
        val cityId = intent.extras?.getInt("City id")
        CurrentCityPresenterFactory(cityId).create()
    }

    private lateinit var temperature: TextView
    private lateinit var weatherImage: ImageView
    private lateinit var header: TextView
    private lateinit var weather: TextView
    private lateinit var wind: TextView
    private lateinit var humidity: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.current_city_activity)
        initViews()
        presenter.attachView(this)
    }

    private fun initViews() {
        header = findViewById(R.id.city_name)
        temperature = findViewById(R.id.temperature)
        weather= findViewById(R.id.weatherType)
        wind = findViewById(R.id.windSpeed)
        humidity = findViewById(R.id.humidity)
        weatherImage = findViewById(R.id.weatherTypeImage)
    }

    override fun bindCity(city: City) {
        weatherImage.setImageResource(city.imageResource)
        header.text = city.name
        temperature.text = getString(R.string.temperature, city.temperature.toString())
        wind.text = getString(R.string.windSpeed, city.windSpeed.toString())
        weather.text = getString(R.string.weatherType, city.weatherType)
        humidity.text = getString(R.string.humidity, city.humidity.toString())
    }

    override fun closeScreen() {
        finish()
    }
}