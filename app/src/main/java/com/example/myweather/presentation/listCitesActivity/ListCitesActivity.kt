package com.example.myweather.presentation.listCitesActivity

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.myweather.data.CitesRepositoryImpl
import com.example.myweather.domain.City
import com.example.myweather.presentation.presenters.ListPresenter
import com.example.myweather.R
import com.example.myweather.presentation.currentCityActivity.CurrentCityActivity
import java.text.DateFormat
import java.util.*


class ListCitesActivity : AppCompatActivity(), CitesListView {

    private val presenter by lazy {
        ListPresenterFactory().create()
    }

    private lateinit var citesList: List<City>
    private lateinit var header: TextView
    private lateinit var subheader: TextView


    private val adapter by lazy {
        CitesListAdapter(citesList, presenter)
    }
    private lateinit var citesListView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_cityes_activity)
        initViews()
        presenter.attachView(this)
        citesListView = findViewById(R.id.recycler_view)
    }

    @Suppress("DEPRECATION")
    private fun initViews() {
        val currentTime: Date = Calendar.getInstance().getTime()
        header = findViewById(R.id.header)
        subheader = findViewById(R.id.date)
        header.text = "Good" + determineTimeOfDay(currentTime.hours)
        subheader.text = DateFormat.getDateInstance(DateFormat.ERA_FIELD).format(currentTime)
    }

    private fun determineTimeOfDay(hour: Int): String {
        return when(hour) {
            in 0..10 -> "\nMorning"
            in 10..17 -> "\nAfternoon"
            else -> "\nEvening"
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onViewResumed()
        citesListView.adapter = adapter
    }

    override fun openCurrentCityScreen(cityId: Int) {
        val intent = Intent(this, CurrentCityActivity::class.java)
        intent.putExtra("City id", cityId)
        startActivity(intent)
    }

    override fun bindCitesList(list: List<City>) {
        citesList = list
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }

}