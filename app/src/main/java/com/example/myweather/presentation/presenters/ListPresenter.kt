package com.example.myweather.presentation.presenters

import com.example.myweather.domain.GetListCitesUseCase
import com.example.myweather.presentation.listCitesActivity.CitesListAdapter
import com.example.myweather.presentation.listCitesActivity.CitesListView

class ListPresenter(private  val getListCitesUseCase: GetListCitesUseCase) : BasePresenter<CitesListView>(), CitesListAdapter.OnItemClickListener {
    override fun onItemClick(position: Int) {
            view?.openCurrentCityScreen(position)
    }

    fun onViewResumed() {
        val personList = getListCitesUseCase()
        view?.bindCitesList(personList)
    }
}