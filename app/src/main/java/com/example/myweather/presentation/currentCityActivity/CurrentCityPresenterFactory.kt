package com.example.myweather.presentation.currentCityActivity

import com.example.myweather.data.CitesLocalDataSourceImpl
import com.example.myweather.data.CitesRepositoryImpl
import com.example.myweather.domain.GetCityUseCase
import com.example.myweather.presentation.presenters.CurrentCityPresenter

class CurrentCityPresenterFactory(private val cityId: Int?) {
    fun create(): CurrentCityPresenter {
        val citesDataSource = CitesLocalDataSourceImpl()
        val citesRepository = CitesRepositoryImpl(citesDataSource)
        val getCityUseCase: GetCityUseCase = GetCityUseCase(citesRepository)
        return CurrentCityPresenter(getCityUseCase, cityId)
    }
}