package com.example.myweather.presentation.currentCityActivity

import com.example.myweather.domain.City
import com.example.myweather.presentation.BaseView

interface CurrentCityView : BaseView {
    fun bindCity(city: City)
    fun closeScreen()
}