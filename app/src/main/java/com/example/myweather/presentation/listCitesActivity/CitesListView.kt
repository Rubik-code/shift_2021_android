package com.example.myweather.presentation.listCitesActivity

import com.example.myweather.domain.City
import com.example.myweather.presentation.BaseView

interface CitesListView: BaseView {
    fun openCurrentCityScreen(cityId: Int)

    fun bindCitesList(list: List<City>)
}