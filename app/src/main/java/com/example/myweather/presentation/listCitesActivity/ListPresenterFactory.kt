package com.example.myweather.presentation.listCitesActivity

import com.example.myweather.data.CitesLocalDataSourceImpl
import com.example.myweather.data.CitesRepositoryImpl
import com.example.myweather.domain.GetCityUseCase
import com.example.myweather.domain.GetListCitesUseCase
import com.example.myweather.presentation.presenters.CurrentCityPresenter
import com.example.myweather.presentation.presenters.ListPresenter

class ListPresenterFactory {
    fun create(): ListPresenter {
        val citesDataSource = CitesLocalDataSourceImpl()
        val citesRepository = CitesRepositoryImpl(citesDataSource)
        val getListCitesUseCase = GetListCitesUseCase(citesRepository)
        return ListPresenter(getListCitesUseCase)
    }
}